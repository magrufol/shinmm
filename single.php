<?php

the_post();
get_header();
$fields = get_fields();


?>


<div class="container mb-5">
    <div class="row about-content">
        <div class="col-12">
            <?php the_content(); ?>
        </div>
    </div>
    <?php if($fields['about_links']): ?>
        <div class="row about-links">
            <div class="col-12">
                <h2>התשוקה שלנו היא חדשנות</h2>
            </div>
            <div class="col-12 links-wrap d-flex justify-content-between">
                <?php foreach ($fields['about_links'] as $link): ?>
                    <div class="ab-link">
                        <a href="<?php the_permalink($link); ?>" style="background-image: url(<?= get_the_post_thumbnail_url($link) ?>)">
                            <p class="ab-title"><?= $link->post_title ?></p>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php

    require_once(THEMEPATH . '/partials/whus.php');
    require_once(THEMEPATH . '/partials/project.php');

    ?>



</div>

<?php get_footer(); ?>
