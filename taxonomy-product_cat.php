<?php

get_header();
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;


$terms[] = $term_id;

if(isset($_GET['cid'])){
    $terms = [];
    $param = $_GET['cid'];
    foreach ($param as $var){
        $terms[] = (int)$var;
    }

}

$categories = get_terms('product_cat', ['hide_empty' => false]);

$products = get_posts([
    'posts_per_page' => -1,
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $terms,
        )
    )
])


?>

<div class="container-fluid term-desc mb-5">
    <div class="row">
        <div class="col-12 cat-desc">
            <?= term_description($queried_object) ?>
        </div>
    </div>
</div>


<div class="container-fluid prod-list-cat mb-5">
    <div class="row px-0">
        <?php if($categories): ?>
            <div class="col-md-3 col-12 px-0">
                <div class="filter">
                    <form action="<?php get_term_link($queried_object); ?>" method="get">
                        <div class="cat-list">
                            <?php foreach ($categories as $cat): ?>
                            <span class="checkbox-wrap">
                                <input type="checkbox"  name="cid[]" value="<?= $cat->term_id ?>" <?=  (in_array($cat->term_id, $terms)) ? 'checked' : '' ?>>
                                <span class="label"><?= $cat->name ?></span>
                            </span>
                            <?php endforeach; ?>
                        </div>
                        <div class="d-flex mt-5">
                            <button type="submit" class="link-btn">
                                סנן
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-9 col-12 products-box">
            <?php if($products): ?>
                <?php foreach ($products as $product): ?>
                    <div class="prod-in-list shadow">
                        <a href="<?php the_permalink($product); ?>">
                            <?php if(has_post_thumbnail($product)): ?>
                                <span class="product-thumb">
                                    <span>
                                        <img src="<?= get_the_post_thumbnail_url($product, 'large') ?>"
                                             alt="<?= $product->post_title ?>" title="<?= $product->post_title ?>">
                                    </span>
                                </span>
                            <span class="prod-info-inb">
                                <span class="name"><?= $product->post_title ?></span>
                                <span class="text-prev"><?= trunc($product->post_content, 10) ?></span>
                            </span>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
                <div class="prod-in-list"></div>
                <div class="prod-in-list"></div>
                <div class="prod-in-list"></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>