<?php
$fields = get_fields(get_option( 'page_on_front' ));
$cmp_img = get_field('company_image', 'option');
$tel = get_field('phone', 'option');
$fax = get_field('fax', 'option');
$mail = get_field('mail', 'option');
$address = get_field('address_title', 'option');
?>


<div class="row wu-row mt-5">
    <div class="col-md-auto col-12 wu-col">
        <h3 class="section-title bordered-no-span">למה לבחור בנו?</h3>
        <?php if($fields['why_us']): ?>
            <div class="why-us">
                <?php foreach ($fields['why_us'] as $wu): ?>
                    <div class="wu-sin">
                                <span>
                                    <img src="<?= $wu['icon']['url'] ?>"
                                         alt="<?= $wu['icon']['alt'] ?>"
                                         title="<?= $wu['icon']['title'] ?>" class="img-fluid">
                                </span>
                        <span class="title"><?= $wu['title'] ?></span>
                    </div>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>
    </div>


    <div class="col-md col-12 contact-details px-0">
        <?php if($cmp_img): ?>
            <div class="cmp-img">
                <img src="<?= $cmp_img['url'] ?>"
                     alt="<?= $cmp_img['alt'] ?>"
                     title="<?= $cmp_img['title'] ?>" class="img-fluid">
            </div>
        <?php endif; ?>
        <div class="contact-info">
            <h4>צרו איתנו קשר</h4>
            <ul>
                <?php if($tel): ?>
                    <li>
                        <span class="ic"> > </span>
                        <span class="cd-title">טלפון:</span>
                        <a href="tel:<?= str_replace('-','', $tel) ?>">
                            <?= $tel ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if($fax): ?>
                    <li>
                        <span class="ic"> > </span>
                        <span class="cd-title">פקס:</span>
                        <a href="<?= $fax ?>">
                            <?= $fax ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if($mail): ?>
                    <li>
                        <span class="ic"> > </span>
                        <span class="cd-title">מייל:</span>
                        <a href="mailto:<?= $mail ?>">
                            <?= $mail ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if($address): ?>
                    <li>
                        <span class="ic"> > </span>
                        <span class="cd-title">כתובתנו:</span>
                        <span><?= $address ?></span>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
