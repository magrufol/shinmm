<?php
$fields = get_fields(get_option( 'page_on_front' ));
?>


<div class="row hp-proj-row">
    <div class="col-12">
        <h3 class="section-title">הפרויקטים שלנו</h3>
    </div>
    <div class="col-12">
        <div class="about-projects">
            <div class="info">
                        <span class="proj-info">
                            <?= $fields['hp_projects_text'] ?>
                        </span>

                <span class="d-flex">
                            <a href="<?= $fields['projects_btn'] ?>" class="link-btn proj">לכל הפרויקטים</a>
                        </span>
            </div>

            <div class="p-image">
                <img src="<?= $fields['hp_projects_img']['url'] ?>"
                     alt="<?= $fields['hp_projects_img']['alt'] ?>"
                     title="<?= $fields['hp_projects_img']['title'] ?>" class="img-fluid">
            </div>
        </div>
    </div>
</div>