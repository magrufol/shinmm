<?php
function custom_login_css() {
echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/login/login-styles.css">';
}
add_action('login_head', 'custom_login_css');

function custom_login_message() {
$message = "<div class='logmsg'>
<h2>נתקלת בבעיה?</h2>
<p>
לתמיכה טכנית יש להתקשר לטלפון 04-8455445
<br>
אפשר לפנות אלינו גם במייל לכתובת: office@leos.co.il
</p>
</div>";
return $message;
}
add_filter('login_message', 'custom_login_message');

function loginpage_custom_link() {
	return 'http://www.leos.co.il/';
}
add_filter('login_headerurl','loginpage_custom_link');

function change_title_on_logo() {
	return 'לאוס מדיה ואינטראקטיב';
}
add_filter('login_headertitle', 'change_title_on_logo');
?>