jQuery(document).ready(function() {
    var ntsave = false;
    var pages = [
        "options-general.php",
        //"options-media.php",
        "options-reading.php",
        "options-discussion.php",
        "options-permalink.php",
        "options-general.php?page=swpsmtp_settings",
        "options-general.php?page=tinymce-advanced",
        "post.php?post=2&action=edit",
        "post.php",
        "admin.php?page=wpseo_dashboard",
        "admin.php?page=wpseo_titles",
        "admin.php?page=wpseo_advanced",
        "users.php",
        "profile.php",
        "options-general.php?page=gallery-options",
        "admin.php?page=toplevel_page_itsec_settings",
        "admin.php?page=toplevel_page_itsec_advanced",
        "admin.php?page=leos_settings&config=done",
        "stop"
    ];
    var loc = jQuery(location);
    var path = loc[0].pathname;
    path = path.split('/');
    path = path[path.length-1];
    var search = loc[0].search;
    search = decodeURIComponent(search);
    search = search.substring(1);
    search = search.split('&');
    var actions = {};
    jQuery.each(search,function(index,val){
        var temp = val.split('=');
        var key = temp[0];
        actions[key] = temp[1];
    });
    //console.log(path);
    //console.log(actions);
    if (actions["page"] === undefined) {
        var curpage = path;
    } else {
        var curpage = actions["page"];
    }
    //alert(curpage);
    //console.log(curpage);

    function nextpage() {
        if (curpage == 'leos_settings') {
            nextpage = 0;
        } else {
            var cp = jQuery.inArray(curpage,pages);
            nextpage = cp + 1;
            if (nextpage == 0) {
                nextpage = jQuery.inArray(path + '?page=' + actions["page"],pages) + 1;
            }
        }
        //console.log(pages[nextpage]);
        if (pages[nextpage] != "stop") {
            window.location.href = pages[nextpage];
        }
    }

    function select(selobj) {
        jQuery.each(selobj, function( key, value ) {
            if (jQuery('#' + key).val() != value) {
                jQuery('#' + key).find('option').each(function( i, opt ) {
                    if (opt.value === value) {
                        jQuery(opt).attr('selected', 'selected');
                    }
                    ntsave = true;
                });
            }
        });

    }

    function checkbox(chobj) {
        jQuery.each(chobj, function(key, value) {
            if (value == 'false') {
                var tatr = jQuery('#' + key).attr("checked");
                if (tatr != undefined && tatr != null) {
                    jQuery('#' + key).removeAttr("checked");
                    ntsave = true;
                }
            } else {
                if (jQuery('#' + key).attr("checked") != value) {
                    jQuery('#' + key).attr("checked", value);
                    ntsave = true;
                }
            }
        });
    }

    function input(inobj) {
        jQuery.each(inobj, function(key, value) {
            var inp = jQuery('input[name="' + key +'"]');
            if (inp.val() != value) {
                inp.val(value);
                ntsave = true;
            }
        });
    }

    if (curpage == 'leos_settings') {
        if (actions["config"] == 'done') {
            jQuery('#set_done').attr("checked","checked");
            jQuery('#submit').trigger( "click" );
        }
        jQuery('#lazy_ass').click(function(){
            nextpage();
        });

        jQuery('#FPON').click(function(){
            window.location.href = "plugins.php";
        });

        jQuery('#UPDATEP').click(function(){
            window.location.href = "update-core.php";
        });
    }

    if (curpage == 'all&paged') {
        window.location.href = "admin.php?page=leos_settings";
    }

    if (curpage == 'about.php') {
        window.location.href = "update-core.php";
    }

    if (curpage == 'do-plugin-upgrade') {
        //window.location.href = "update-core.php";
    }

    if(curpage == 'post.php' && actions["post"] == 2) {
        var wait = 1;
        if (jQuery('#title').val() != 'עמוד הבית') {
            jQuery('#title').val('עמוד הבית');
            ntsave = true;
        }
        var selobj = {
            "page_template": "template/page-front.php",
        }
        select(selobj);
        jQuery( document ).on( 'tinymce-editor-init', function( event, editor ) {
           tinyMCE.activeEditor.setContent('');
           wait = 1;
        });
        if (ntsave) {
            jQuery('#publish').trigger( "click" );
        } else if (wait) {
            nextpage();
        }
    }

    if(curpage == 'options-media.php') {
        var s = '250';
        var m = '550';
        if (jQuery('#thumbnail_size_w, #thumbnail_size_h').val() != s || jQuery('#medium_size_w, #medium_size_h').val() != m) {
            jQuery('#thumbnail_size_w, #thumbnail_size_h').val(s);
            jQuery('#medium_size_w, #medium_size_h').val(m);
            ntsave = true;
        }
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if(curpage == 'plugins.php') {
        var plugson = jQuery('.subsubsub .active .count').text();
        plugson = plugson.replace('(','');
        plugson = plugson.replace(')','');
        plugson = parseInt(plugson);
        if (plugson >= 10) {
            window.location.href = "options-general.php?page=leos_settings";
        } else {
            var chobj = {
                "advanced-custom-fields-pro/acf.php": "checked",
                "contact-form-7/wp-contact-form-7.php": "checked",
                "contact-form-7-to-database-extension/contact-form-7-db.php": "checked",
                "easy-wp-smtp/easy-wp-smtp.php": "checked",
                "gallery-manager-pro/plugin.php": "checked",
                "better-wp-security/better-wp-security.php": "checked",
                "jquery-updater/jquery-updater.php": "checked",
                "tinymce-advanced/tinymce-advanced.php": "checked",
                "wordpress-seo/wp-seo.php": "checked",
                "post-thumbnail-editor/post-thumbnail-editor.php": "checked",
            };

            jQuery('.plugins #the-list input').each(function() {
                var temp = jQuery(this);
                jQuery.each(chobj, function(key, value) {
                    if (temp.val() == key) {
                        temp.attr("checked", value);
                        ntsave = true;
                    }
                });
            })

            if (ntsave) {
                var selobj = {
                    "bulk-action-selector-bottom": "activate-selected",
                }
                select(selobj);
                jQuery('#doaction2').trigger( "click" );
            }
        }
    }

    if(curpage == 'update-core.php') {
        if (jQuery('#upgrade').length == 1 && jQuery('#upgrade').val() != 'התקן מחדש') {
            jQuery('#upgrade').trigger( "click" );
        } else if (jQuery('.plugins tr').length > 0) {
            setTimeout(function(){ jQuery('#plugins-select-all').trigger( "click" ); jQuery('#plugins-select-all').attr('checked', 'checked');  },1000);
            setTimeout(function(){ jQuery('#upgrade-plugins-2').trigger( "click" ); },3000);
        }
    }

    if(curpage == 'options-general.php') {
        if (jQuery('#blogdescription').val() != '') {
            jQuery('#blogdescription').val('');
            ntsave = true;
        }

        if (jQuery('#timezone_string').val() != 'Asia/Jerusalem') {
            jQuery('#timezone_string').find('option').each(function( i, opt ) {
                if (opt.value === 'Asia/Jerusalem') {
                    jQuery(opt).attr('selected', 'selected');
                    ntsave = true;
                }
            });
        }

        if (jQuery('#start_of_week').val() != '0') {
            jQuery('#start_of_week').find('option').each(function( i, opt ) {
                if (opt.value === '0') {
                    jQuery(opt).attr('selected', 'selected');
                    ntsave = true;
                }
            });
        }

        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'options-reading.php') {
        if (jQuery('input:radio[name="show_on_front"]').eq(1).attr("checked") != 'checked') {
            jQuery('input:radio[name="show_on_front"]').eq(1).attr("checked", 'checked');
            ntsave = true;
        }
        if (jQuery('#page_on_front').val() != '2') {
            jQuery('#page_on_front').val('2');
            ntsave = true;
        }
        var inobj = {
            "posts_per_page": "999",
            "posts_per_rss": "999",
        }
        input(inobj);

        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }

    }

    if (curpage == 'options-discussion.php') {
        var chobj = {
            "default_comment_status": "false",
            "comments_notify": "false",
            "moderation_notify": "false",
        };
        checkbox(chobj);
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'options-permalink.php') {
        if (jQuery('input:radio[name="selection"][value="/%postname%/"]').attr("checked") != 'checked') {
            jQuery('input:radio[name="selection"][value="/%postname%/"]').attr("checked", 'checked');
            ntsave = true;
        }
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'swpsmtp_settings') {
        var loc = window.location.pathname;
        var from = loc.split('/')[1];
        from = from.replace("~","");
        var inobj = {
            "swpsmtp_from_email": "no-reply@leos.co.il",
            "swpsmtp_from_name": from,
            "swpsmtp_smtp_host": "smtp.gmail.com",
            "swpsmtp_smtp_port": "587",
            "swpsmtp_smtp_username": "no-reply@leos.co.il",
            "swpsmtp_smtp_password": "123456leos",
        }
        input(inobj);

        if (jQuery('#swpsmtp_smtp_type_encryption_3').attr("checked") != 'checked') {
            jQuery('#swpsmtp_smtp_type_encryption_3').attr("checked", 'checked');
            ntsave = true;
        }

        if (ntsave) {
            jQuery('#settings-form-submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'tinymce-advanced') {
        function delayz() {
            var chobj = {
                "no_autop": "checked",
                "fontsize_formats": "checked",
            };
            checkbox(chobj);
            if (ntsave) {
                jQuery('.button-primary').eq(1).trigger( "click" );
            } else {
                nextpage();
            }
        }
        setTimeout(delayz(),1000)
    }

    if (curpage == 'wpseo_dashboard') {
        nextpage();
    }

    if (curpage == 'wpseo_titles') {
        var chobj = {
            "forcerewritetitle": "checked",
        };
        checkbox(chobj);
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'wpseo_advanced') {
        var chobj = {
            "breadcrumbs-enable": "checked",
        };
        checkbox(chobj);
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'users.php') {
        var user = jQuery('#the-list tr').eq(0).find('.username a').eq(0).attr('href');
        user = decodeURIComponent(user);
        user = user.replace('users.php?nextpage', "users.php&nextpage");
        if (user != undefined && user != null) {
            window.location.href = user;
        }
    }

    if (curpage == 'profile.php' && jQuery('#admin_bar_front').length == 1) {
        var user = jQuery('#nickname').val();
        if (user.indexOf("SU") >= 0) {
            user = user.split('SU')[0];
            jQuery('#nickname').val(user);
            jQuery('#display_name').append('<option>' + user + '</option>');
            ntsave = true;
        }
        if (jQuery('#display_name').val() != user) {
            jQuery('#display_name').find('option').each(function( i, opt ) {
                if (opt.value === user) {
                    jQuery(opt).attr('selected', 'selected');
                    ntsave = true;
                }
            });
        }
        var box = jQuery('#admin_bar_front').attr('checked');
        if (box != undefined && box != null) {
            jQuery('#admin_bar_front').removeAttr('checked');
            ntsave = true;
        }
        if (ntsave) {
            jQuery('#submit').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'gallery-options') {
        var user = 'invoice@leos.co.il';
        var pass = 'ff86bbedcbe6';
        if (jQuery('#update_username').val() != user) {
            jQuery('#update_username').val(user);
            jQuery('#update_password').val(pass);
            ntsave = true;
        }
        if (ntsave) {
            jQuery('.button-primary').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'toplevel_page_itsec_settings') {
        var chobj = {
            "itsec_global_write_files": "checked",
            "itsec_four_oh_four_enabled": "checked",
            "itsec_global_hide_admin_bar": "checked",
            "itsec_ban_users_default": "checked",
            "itsec_ban_users_enabled": "checked",
            "itsec_brute_force_enabled": "checked",
            "itsec_hide_backend_enabled": "checked",
            "itsec_strong_passwords_enabled": "checked",
            "itsec_tweaks_server_protect_files": "checked",
            "itsec_tweaks_server_directory_browsing": "checked",
            "itsec_tweaks_server_request_methods": "checked",
            "itsec_tweaks_server_suspicious_query_strings": "checked",
            "itsec_tweaks_server_uploads_php": "checked",
            "itsec_tweaks_server_wlwmanifest_header": "checked",
            "itsec_tweaks_server_edituri_header": "checked",
            "itsec_tweaks_server_comment_spam": "checked",
            "itsec_tweaks_server_file_editor": "checked",
            "itsec_tweaks_server_force_unique_nicename": "checked",
            "itsec_tweaks_server_disable_unused_author_pages": "checked",
            "itsec_brute_force_auto_ban_admin": "checked",
            "itsec_global_lock_file": "checked",
        };
        checkbox(chobj);

        var inobj = {
            "itsec_hide_backend[slug]": "leos-login",
            "itsec_brute_force[max_attempts_host]": "3",
            "itsec_brute_force[max_attempts_user]": "5",
            "itsec_brute_force[check_period]": "30",
        }
        input(inobj);

        var selobj = {
            "itsec_strong_passwords_roll": "subscriber",
            "itsec_tweaks_server_disable_xmlrpc": "2",
        }
        select(selobj);

        if (ntsave) {
            jQuery('#tweaks_wordpress .button-primary').trigger( "click" );
        } else {
            nextpage();
        }
    }

    if (curpage == 'toplevel_page_itsec_advanced') {
        if (jQuery('#itsec_enable_admin_user').length > 0) {
            var chobj = {
                "itsec_enable_admin_user": "checked",
                "itsec_admin_user_id": "checked",
            };
            checkbox(chobj);
        }
        if (ntsave) {
            jQuery('#admin_user_options .button-primary').trigger( "click" );
        } else {
            nextpage();
        }
    }
});