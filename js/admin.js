var ntsave = false;
var loc = jQuery(location);
var path = loc[0].pathname;
path = path.split('/');
path = path[path.length-1];
var search = loc[0].search;
search = decodeURIComponent(search);
search = search.substring(1);
search = search.split('&');
var actions = {};
jQuery.each(search,function(index,val){
    var temp = val.split('=');
    var key = temp[0];
    actions[key] = temp[1];
});
if (actions["nextpage"] === undefined ) {
    var nextpage = 0;
} else {
    var nextpage = actions["nextpage"];
}
//console.log(path);
//console.log(actions);
if (actions["page"] === undefined) {
    var curpage = path;
} else {
    var curpage = actions["page"];
}
jQuery(document).ready(function() {
    //console.log(curpage);
    if (curpage == 'wpcf7') {
        jQuery('#postbox-container-2 p.submit').append('<button id="MBG">Generate</button>');
        var r = /<(\w+)[^>]*>.*<\/\1>/gi;
        var h2s = 'font-size:20px;';
        var ss = 'color:#2155da;';
        var contacts = '';
        var change = 0;
        var mail = '';
        jQuery('#MBG').on('click',function(e){
            e.preventDefault();
            mail = '';
            contacts = '';
            jQuery('#wpcf7-mail-body').val('');
            jQuery('#wpcf7-mail-body').html('');
            var text = jQuery('#wpcf7-form').val().toLowerCase().replace(/<\/?[^>]+(>|$|)|"/g, "");
            text = text.replace(/[[\]]|[*]|:+/g,'');
            var words = text.split(/\s+/);
            jQuery.each( words, function( key, value ) {
                if(value == 'placeholder') {
                    contacts += '    <h2 style="' + h2s + '"><strong style="' + ss + '">' + words[key + 1] + ':</strong> [' + words[key - 1] + '] </h2>\n';
                }
            });
            mail = '<div dir="rtl">\n' + contacts + '</div>';
            jQuery('#wpcf7-mail-body').val(mail);
            jQuery('#wpcf7-mail-subject').val('פניה חדשה מהאתר [_date] [_time]');
            jQuery('#wpcf7-mail-use-html').attr("checked", "checked");
            jQuery('#mail-panel-tab a.ui-tabs-anchor').trigger('click');
        });
    }
});
function save() {
    if (jQuery('#publish.button-primary').length > 0) {
        jQuery('#publish.button-primary').trigger('click');
    } else if (jQuery('#submit.button-primary').length > 0) {
        jQuery('#submit.button-primary').trigger('click');
    } else {
        jQuery('input[type=submit].button-primary').trigger('click');
    }
}
jQuery(document).ready(function() {
	jQuery(document).keyup(function(e) {
		if(e.which == 17) {
			isCtrl = false;
		}
		if(e.which == 16) {
			isShift = false;
		}
	});
	jQuery(document).keydown(function(e) {
		if(e.which == 17) {
			isCtrl = true;
		}
		if(e.which == 16) {
			isShift = true;
		}
		if(e.which == 83 && isCtrl && isShift) {
            save();
		}
	});
});