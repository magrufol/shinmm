<?php

get_header();
$fields = get_fields();

?>

<div class="container">
    <div class="row justify-content-center mt-3 mb-5">
        <div class="col-md-9 col-12">
            <div class="product-preview">
                <div class="info">
                    <div class="info-content">
                        <?php the_content(); ?>
                        <div class="btn-wrap d-flex">
                            <a href="#" id="scroll-to-spec" class="link-btn">
                                למפרטים
                            </a>
                        </div>
                    </div>
                    <?php if($fields['product_image']): ?>
                        <div class="images-thumbs">
                            <?php foreach ($fields['product_image'] as $img): ?>
                                <div class="thumb-btn"
                                     data-src="<?= $img['url'] ?>"
                                     style="background-image: url(<?= $img['sizes']['thumbnail'] ?>)"
                                     data-thumb="<?= $img['sizes']['thumbnail'] ?>">
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="image-focus">
                    <span>
                        <img id="large-img" data-thumb="<?= get_the_post_thumbnail_url() ?>" src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title() ?>" class="img-fluid">
                    </span>
                </div>
            </div>
        </div>
    </div>


    <?php if($fields['product_about']): ?>
        <div class="row prod-info-row">
            <div class="col-md-3 col-12">
                <h6>תיאור מוצר</h6>
            </div>
            <div class="col-md-9 col-12 pir-content">
                <?= $fields['product_about'] ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if($fields['product_spec']): ?>
        <div class="row prod-info-row">
            <div class="col-md-3 col-12">
                <h6>נתוני המוצר</h6>
            </div>
            <div class="col-md-9 col-12 pir-content">
                <?= $fields['product_spec'] ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if($fields['product_manual']): ?>
        <div class="row prod-info-row">
            <div class="col-md-3 col-12">
                <h6>אופן השימוש</h6>
            </div>
            <div class="col-md-9 col-12 pir-content">
                <?= $fields['product_manual'] ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if($fields['product_standarts']): ?>
        <div class="row prod-info-row">
            <div class="col-md-3 col-12">
                <h6>תקנים ומפרטים</h6>
            </div>
            <div class="col-md-9 col-12 pir-content">
                <?= $fields['product_standarts'] ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if($fields['prod_links']): ?>
        <div class="row prod-links">
            <?php foreach ($fields['prod_links'] as $link): ?>
                <div class="col-md col-12">
                    <a href="<?= $link['link'] ?>" target="_blank" title="<?= $link['title'] ?>">
                        <span class="icon"><img src="<?= $link['icon']['url'] ?>"
                                                alt="<?= $link['icon']['alt'] ?>"
                                                title="<?= $link['icon']['title'] ?>">
                        </span>

                        <span class="text">
                            <span class="bld mb-3"><?= $link['title'] ?></span>
                            <span><?= $link['sub_title'] ?></span>
                        </span>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="row product-accordion">
        <div class="col-12">
            <div class="accordion" id="product-accordion">
                <?php if($fields['product_tech']): ?>
                <div class="down-card">
                    <div class="down-card-header" id="headingOne">
                        <h2 class="mb-0">
                            <span data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                מידע טכני
                            </span>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <?= $fields['product_tech'] ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($fields['product_down']): ?>
                <div class="down-card">
                    <div class="down-card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <span  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                קבצים להורדה
                            </span>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <?php foreach ($fields['product_down'] as $d): ?>
                                <a href="<?= $d['link'] ?>" download target="_blank">
                                    <?= $d['title']  ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php get_footer(); ?>
