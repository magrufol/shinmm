<?php
global $my_lang,$phone,$mail,$address,$socials,$isMobile,$c_page;

$map_img = get_field('map', 'options');
$icons = get_field('footer_icons', 'options');
$fb = get_field('fb_link', 'options');
$waze = get_field('waze_link', 'options');
$youtube = get_field('youtube_link', 'options');
?>
</div><!--#content end-->
<footer>

    <div class="footer-top">

        <div class="scroll-to-top">
            <img src="<?= ICON . 'top.png' ?>" alt="">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h5 class="form-title">
                        <span class="big">מעוניינים בשירותינו?</span>
                        <span>השאירו פרטיכם ונחזור אליכם בהקדם</span>
                    </h5>
                </div>
                <div class="col-12">
                    <?php echo do_shortcode('[contact-form-7 id="5" title="צור קשר"]'); ?>
                </div>
            </div>
            <div class="row mt-5 justify-content-between">
                <div class="col-md-auto col-12 footer-nav">
                    <h5 class="footer-title pl-5">תפריט ניווט</h5>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-menu', 'depth' => '1', 'container_class' => 'footer-nav', 'menu_class' => '') ); ?>
                </div>

                <div class="col-md-auto col-12 footer-nav two-col">
                    <h5 class="footer-title pl-5">תפריט עזר </h5>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => '1', 'container_class' => 'footer-nav', 'menu_class' => '') ); ?>
                </div>

                <div class="col-md-auto col-12">
                    <?php if($map_img): ?>
                        <h5 class="footer-title pl-5">אנחנו פה</h5>
                        <div class="map-img">
                            <img src="<?= $map_img['url'] ?>"
                                 alt="<?= $map_img['alt'] ?>"
                                 title="<?= $map_img['title'] ?>"
                                 class="img-fluid">
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom">


        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <div class="site-name">ש.מ. מיגון ובטיחות</div>
                </div>

                <div class="col-auto">
                    <div class="icons">
                        <?php if($icons): ?>
                            <?php foreach ($icons as $i): ?>
                                <div class="ficon">
                                    <img src="<?= $i['url'] ?>" alt="<?= $i['alt'] ?>" title="<?= $i['title'] ?>">
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if($waze): ?>
                            <a href="<?= $waze?>" class="ficon custom waze">
                                <img src="<?= ICON . 'waze.jpg' ?>" alt="Waze" title="waze">
                            </a>
                        <?php endif; ?>

                        <?php if($fb): ?>
                            <a href="<?= $fb ?>" class="ficon custom fb">
                                <img src="<?= ICON . 'fb.png' ?>" alt="Facebook" title="Facebook">
                            </a>
                        <?php endif; ?>

                        <?php if($youtube): ?>
                            <a href="<?= $youtube ?>" class="ficon custom youtube">
                                <img src="<?= ICON . 'youb.png' ?>" alt="Youtube" title="Youtube">
                            </a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    </div>

</footer>

<div id="leos">
	<a href="http://www.leos.co.il/" title="">
		<img src="<?php echo bloginfo('template_url');?>/images/leoslogo.png" alt="" />
		<span></span>
	</a>
</div>



</div> <!-- close wrapper -->
<?php wp_footer(); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1840068962951769&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
