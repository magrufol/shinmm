<!DOCTYPE HTML>
<html <?php language_attributes(); ?> dir="ltr">
<?php global $my_lang,$sticky,$isMobile,$phone,$mail,$address,$socials,$logo,$c_seo,$c_page; $c_page = c_page(); ?>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php if ($c_page['page'] == 'search') { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<link rel="icon" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>" type="image/png" />
	<?php if (0) { ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Amatica+SC:400,700&amp;subset=hebrew" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Assistant:300,400,600,700&amp;subset=hebrew" rel="stylesheet">
	<?php } ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/hover.css'; ?>" type="text/css"media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick/slick.css"/>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick/slick-theme.css"/>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<?php if ($c_seo) { echo $c_seo; } ?>
</head>

<body <?php body_class(); ?>>
<?php if ($c_page['is_tax']) {
    $term_id = $c_page['id'];
	$tax = $c_page['type'];
    $bbg = get_field('thebg',$tax.'_'.$term_id);
} else {
    $bbg = get_field('thebg');
} if (empty($bbg)) { $bbg = get_template_directory_uri().'/images/default.jpg'; } ?>
<div id="wrapper">
<header>
	<div class="container-fluid">
        <div class="row align-items-end">
            <div class="logo col-auto">
                <a href="<?php echo get_option('home'); ?>" title="<?php bloginfo('name'); ?>">
                    <?php if ($logo) { ?><img src="<?php echo $logo['url']; ?>" alt="<?php bloginfo('name'); ?>" /><?php } else { ?>
                        <img class="w50" src="<?php echo get_template_directory_uri().'/images/leoslogo.png'; ?>" alt="<?php bloginfo('name'); ?>" />
                    <?php }?>
                </a>
            </div>
            <div class="col-auto d-flex">
                <nav id="MainNav" class="">
                    <div id="MobNavBtn"><span></span><span></span><span></span></div>
                    <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'depth' => '1', 'container_class' => 'main_menu', 'menu_class' => '') ); ?>
                </nav>
            </div>
            <div class="search-btn">
                <img src="<?= ICON . 'search.png' ?>" alt="">
            </div>

            <div class="blog-link">
                <a href="<?= get_field('blog_link', 'options'); ?>">
                    מאמרים
                </a>
            </div>
            <div class="phone">
                <img src="<?= ICON . 'phone.png' ?>" alt="טלפון">
                <a href="<?= $phone['tel'] ?>"><?= $phone['txt'] ?></a>
            </div>
        </div>
	</div>

    <div class="contact-btn">
        <span>צרו איתנו קשר</span>
        <div class="btn-triangle"></div>
    </div>
</header>

    <div class="top-contact-form">
        <?php echo do_shortcode('[contact-form-7 id="206" title="צור קשר"]'); ?>
    </div>

    <?php

    $img = [
            'url' => '',
            'alt' => '',
            'title' => ''
    ];

    if(!is_front_page()):

        $title = get_the_title();
        $img['url'] = get_the_post_thumbnail_url(get_the_ID());


        if(is_category() || is_tax()){
            $obj = get_queried_object();
            $img = get_field('header_img', $obj);
            $title = $obj->name;
        }

        if(is_singular('product') || is_single()){
            $terms = get_the_terms(get_the_ID(), 'product_cat');
            $img['url'] = get_field('header_img', $terms[0])['url'];
        }

        ?>




        <div class="inner-page-header">
            <div class="header-image">
                <img src="<?= $img['url'] ?>" alt="<?= $img['alt'] ?>" title="<?= $img['title'] ?>">
            </div>
            <div class="title-wrap">
                <h1><?= $title ?></h1>
            </div>
        </div>
    <?php endif; ?>


<div id="content">
