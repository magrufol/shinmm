<?php
/*
Template Name: עמוד בית
*/

wp_enqueue_script('slick', JS . '/slick.min.js', false, '', true);
wp_enqueue_style('slick', CSS . '/slick/slick.css');
the_post();
get_header();
$fields = get_fields();
$cmp_img = get_field('company_image', 'option');
$tel = get_field('phone', 'option');
$fax = get_field('fax', 'option');
$mail = get_field('mail', 'option');
$address = get_field('address_title', 'option');

?>

<?php if($fields['hp_slider']): ?>
    <section class="home-slider">
        <div class="images-slider">
            <?php foreach($fields['hp_slider'] as $slide): ?>
                <div class="slide">
                    <img src="<?= $slide['image']['url'] ?>"
                         alt="<?= $slide['image']['alt'] ?>"
                         title="<?= $slide['image']['title'] ?>">


                    <div class="slide-text">
                        <span><?= $slide['text'] ?></span>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>

<section class="home-content">
    <div class="container">
        <div class="row">
            <div class="col-12 top-content d-flex flex-column align-items-start">
                <?php the_content(); ?>

                <?php if($fields['more_info_lilnk']): ?>
                    <a href="<?= $fields['more_info_lilnk'] ?>" title="מידע נוסף" class="link-btn info">
                        מידע נוסף
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <?php if($fields['about_items']): ?>
            <div class="row justify-content-between info-items">
                <?php foreach ($fields['about_items'] as $item): ?>
                    <div class="col-md mb-md-0 mb-3 col-6 d-flex flex-column align-items-center justify-content-between">
                        <p class="item-title"><?= $item['title'] ?></p>

                        <span>
                            <img src="<?= $item['icon']['url'] ?>"
                                 alt="<?= $item['icon']['alt'] ?>"
                                 title="<?= $item['icon']['title'] ?>"
                                 class="img-fluid">
                        </span>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if($fields['fav_products']): ?>
            <div class="row products-slider fav-products mb-5">
                
                <div class="col-12 flex-md-row flex-column d-flex justify-content-between align-items-center title-wrap">
                    <h3 class="fav-title"><?= $fields['popular_title'] ?></h3>

                    <a href="<?= $fields['products_link'] ?>" title="לכל המוצרים" class="link-btn fav">לכל המוצרים</a>
                </div>
                
                <div class="col-12">
                    <div class="slider-container">
                        <?php foreach ($fields['fav_products'] as $product): ?>
                            <div class="product-item m-3">
                                <a href="<?php the_permalink($product); ?>" title="<?= $product->post_title ?>">
                                    <?php if(has_post_thumbnail($product)): ?>
                                        <span class="product-thumb">
                                            <img src="<?= get_the_post_thumbnail_url($product, 'large') ?>"
                                                 alt="<?= $product->post_title ?>" title="<?= $product->post_title ?>">
                                        </span>
                                    <?php endif; ?>
                                    <p class="product-title"><?= $product->post_title ?></p>
                                    <span class="product-about">
                                        <?= trunc($product->post_content, 20) ?>
                                    </span>
                                    <span class="product-btn">מידע נוסף</span>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($fields['add_slider_prod']): ?>
            <div class="row products-slider fav-products">

                <div class="col-12 flex-md-row flex-column d-flex justify-content-between align-items-center title-wrap">
                    <h3 class="fav-title"><?= $fields['prod_slider_title'] ?></h3>

                    <a href="<?= $fields['app_link'] ?>" title="לכל המוצרים" class="link-btn fav">לכל המוצרים</a>
                </div>

                <div class="col-12">
                    <div class="slider-container">
                        <?php foreach ($fields['add_slider_prod'] as $product): ?>
                            <div class="product-item m-3">
                                <a href="<?php the_permalink($product); ?>" title="<?= $product->post_title ?>">
                                    <?php if(has_post_thumbnail($product)): ?>
                                        <span class="product-thumb">
                                            <img src="<?= get_the_post_thumbnail_url($product, 'large') ?>"
                                                 alt="<?= $product->post_title ?>" title="<?= $product->post_title ?>">
                                        </span>
                                    <?php endif; ?>
                                    <p class="product-title"><?= $product->post_title ?></p>
                                    <span class="product-about">
                                        <?= trunc($product->post_content, 20) ?>
                                    </span>
                                    <span class="product-btn">מידע נוסף</span>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row hp-proj-row">
            <div class="col-12">
                <h3 class="section-title">הפרויקטים שלנו</h3>
            </div>
            <div class="col-12">
                <div class="about-projects">
                    <div class="info">
                        <span class="proj-info">
                            <?= $fields['hp_projects_text'] ?>
                        </span>

                        <span class="d-flex">
                            <a href="<?= $fields['projects_btn'] ?>" class="link-btn proj">לכל הפרויקטים</a>
                        </span>
                    </div>

                    <div class="p-image">
                        <img src="<?= $fields['hp_projects_img']['url'] ?>"
                             alt="<?= $fields['hp_projects_img']['alt'] ?>"
                             title="<?= $fields['hp_projects_img']['title'] ?>" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>


        <?php if($fields['text_slider']): ?>
            <div class="row">
                <div class="col-12">
                    <div class="seo-slider">
                        <?php foreach ($fields['text_slider'] as $ts): ?>
                            <div class="seo-content-wrap">
                                <h3 class="section-title bordered">
                                    <span><?= $ts['title'] ?></span>
                                </h3>

                                <div class="content"><?= $ts['text'] ?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row wu-row">
            <div class="col-auto wu-col">
                <h3 class="section-title bordered-no-span">למה לבחור בנו?</h3>
                <?php if($fields['why_us']): ?>
                    <div class="why-us">
                        <?php foreach ($fields['why_us'] as $wu): ?>
                            <div class="wu-sin">
                                <span>
                                    <img src="<?= $wu['icon']['url'] ?>"
                                           alt="<?= $wu['icon']['alt'] ?>"
                                           title="<?= $wu['icon']['title'] ?>" class="img-fluid">
                                </span>
                                <span class="title"><?= $wu['title'] ?></span>
                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>
            </div>


            <div class="col contact-details px-0">
                <?php if($cmp_img): ?>
                    <div class="cmp-img">
                        <img src="<?= $cmp_img['url'] ?>"
                             alt="<?= $cmp_img['alt'] ?>"
                             title="<?= $cmp_img['title'] ?>" class="img-fluid">
                    </div>
                <?php endif; ?>
                <div class="contact-info">
                    <h4>צרו איתנו קשר</h4>
                    <ul>
                        <?php if($tel): ?>
                            <li>
                                <span class="ic"> > </span>
                                <span class="cd-title">טלפון:</span>
                                <a href="tel:<?= str_replace('-','', $tel) ?>">
                                    <?= $tel ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($fax): ?>
                            <li>
                                <span class="ic"> > </span>
                                <span class="cd-title">פקס:</span>
                                <a href="<?= $fax ?>">
                                    <?= $fax ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($mail): ?>
                            <li>
                                <span class="ic"> > </span>
                                <span class="cd-title">מייל:</span>
                                <a href="mailto:<?= $mail ?>">
                                    <?= $mail ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($address): ?>
                            <li>
                                <span class="ic"> > </span>
                                <span class="cd-title">כתובתנו:</span>
                                <span><?= $address ?></span>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>


        <?php if($fields['clients']): ?>
            <div class="row clients">
                <div class="col-12">
                    <h3 class="section-title bordered">
                        <span>הלקוחות שלנו</span>
                    </h3>
                </div>
                <div class="col-12">
                    <div class="clients-slider">
                        <?php foreach ($fields['clients'] as $c): ?>
                            <div class="cli-slide">
                                <span>
                                    <img src="<?= $c['url'] ?>"
                                         alt="<?= $c['alt'] ?>"
                                         title="<?= $c['title'] ?>" class="img-fluid">
                                </span>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</section>





<?php get_footer(); ?>
<script>
$(document).ready(function() {
    $('.images-slider').slick({
        rtl: true,
        slidesToShow: 1,
        pauseOnHover: false,
        autoplay: true,
        dots: false,
        fade: true,
        speed: 3000,
        autoplaySpeed: 3000,
    });

    $('.fav-products .slider-container').slick({
        rtl: true,
        slidesToShow: 4,
        pauseOnHover: false,
        autoplay: false,
        dots: false,
        arrows: true,
        prevArrow: '<span class="prod-nav prev"></span>',
        nextArrow: '<span class="prod-nav next"></span>',
        responsive: [
            {
                breakpoint: 1090,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            },
        ]
    });

    $('.seo-slider').slick({
        rtl: true,
        slidesToShow: 1,
        dots: true,
        arrows: false,
    });

    $('.clients-slider').slick({
        rtl: true,
        slidesToShow: 6,
        dots: false,
        arrows: true,
        prevArrow: '<span class="cli-nav prev"></span>',
        nextArrow: '<span class="cli-nav next"></span>',
        responsive: [
            {
                breakpoint: 1090,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            },
        ]
    });

});
</script>
