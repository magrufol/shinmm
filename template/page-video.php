<?php
/*
Template Name: סרטונים
*/
wp_enqueue_script('slick', JS . '/slick.min.js', false, '', true);
wp_enqueue_style('slick', CSS . '/slick/slick.css');
get_header();
$fields = get_fields();
?>


<div class="container mb-5">
    <div class="row about-content">
        <div class="col-12">
            <?php the_content(); ?>
        </div>
    </div>
    <?php if($fields['about_links']): ?>
        <div class="row about-links">
            <div class="col-12">
                <h2>התשוקה שלנו היא חדשנות</h2>
            </div>
            <div class="col-12 links-wrap d-flex justify-content-between">
                <?php foreach ($fields['about_links'] as $link): ?>
                    <div class="ab-link">
                        <a href="<?php the_permalink($link); ?>" style="background-image: url(<?= get_the_post_thumbnail_url($link) ?>)">
                            <p class="ab-title"><?= $link->post_title ?></p>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>



    <?php if($fields['video']): ?>
        <div class="row video">
            <div class="col-12">
                <div class="video-slider">
                    <?php foreach ($fields['video'] as $v): ?>
                        <div class="video-slide" data-title="<?= $v['title']  ?>" data-id="<?= getYoutubeId($v['url']) ?>">
                            <div class="video-thumb"
                                 data-toggle="modal"
                                 style="background-image: url(<?= getYoutubeTumb($v['url']) ?>)"
                                 data-target="#videoModal">

                                <p class="video-title"><?= $v['title']  ?></p>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>



    <?php

    require_once(THEMEPATH . '/partials/project.php');
    require_once(THEMEPATH . '/partials/whus.php');

    ?>


    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="video-title"></h5>
                </div>
                <div class="modal-body">
                    <iframe id="youtube-frame" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php get_footer(); ?>

<script>

    $('.video-slide').click(function () {
        var url =  'https://www.youtube.com/embed/' + $(this).data('id');
        $('#youtube-frame').attr('src', url);
        $('#videoModal').modal();

        $('#video-title').text($(this).data('title'));
    });

    $('#videoModal').on('hide.bs.modal', function (e) {
        $('#youtube-frame').attr('src', '');

        $('#video-title').text('');
    });


    $('.video-slider').slick({
        rtl: true,
        slidesToShow: 4,
        dots: false,
        arrows: true,
        prevArrow: '<span class="vid-nav prev"></span>',
        nextArrow: '<span class="vid-nav next"></span>',
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            },
        ]
    });
</script>