<?php

/*
Template Name: מאמרים
*/

get_header();
$query = new WP_Query(['posts_per_page'   => -1,
    'post_type'        => 'post',]);
?>

<div class="container-fluid archive-container">
    <div class="row">
        <?php if($query->have_posts()): ?>
            <?php while($query->have_posts()): $query->the_post(); ?>
                <div class="col-md-6 col-12">
                    <a href="<?php the_permalink(); ?>" class="post-link">
                        <span class="post-preview">
                            <h5><?php the_title() ?></h5>

                            <span class="text"><?= trunc(get_the_content(), 90); ?></span>
                        </span>
                        <span class="post-thumb"
                              style="background-image: url(<?= get_the_post_thumbnail_url() ?>)" title="<?php the_title() ?>">

                        </span>
                    </a>
                </div>
                <?php wp_reset_postdata(); endwhile; ?>
        <?php endif; ?>
    </div>
</div>


<?php get_footer(); ?>
